"""
This package features two different ways of calculation emission factors for the
german electricity mix.

--- CO2ExAnte ---
# from CO2EmissionTool.models import CO2ExAnte

This class is used for creating day-ahead forecasts of emission factors in the
german electricity mix. The forecast of GHG emissions in the electricity mix is
performed using machine learning techniques. For this purpose a Random Forest
Regressor has been trained using historical day-ahead forecasts of wind and
photovoltaic generation (provided by the ENTSO-E Transparency Platform) as well
as electricity prices as well as temporal features like the day of the week.
Provided with up-to-date forecasts and additional features, the pre-trained
model is able to predict GHG-emissions in the german electricity mix for the
next day. The Notebook containing the code used for training the model is also
availible in this repository.


--- CO2ExPost ---
# from CO2EmissionTool.models import CO2ExPost

Emission factors (ex post) are calculated by offsetting the amount of actually
produced electricity by energy source (provided by the ENTSO-E Transparency
Platform) against the specific emissions per energy source. The specific
emission factors already account for the pre-chain, which is why wind energy,
for example, also has a certain carbon footprint.
"""

from setuptools import setup

print(__doc__)
__version__ = "1.0"
__author__ = "Joachim Ferstl"
__email__ = "jferstl@ffe.de"
__url__ = "https://gitlab.com/ffe-munich/co2emissiontool"

setup(
    packages=['CO2EmissionTool'],
    keywords=['CO2', 'forecast', 'random forest'],
    install_requires=[
        "requests",
        "pandas",
        "numpy",
        "bs4",
        "arrow",
        "scikit-learn"
    ],
    include_package_data=True,
    name='CO2EmissionTool',
    long_description=__doc__,
    long_description_content_type="text/markdown",
    version=__version__,
    author=__author__,
    author_email=__email__,
    maintainer=__author__,
    maintainer_email=__email__,
    url=__url__,
    download_url=__url__,
    platforms=["any"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: CC0 1.0 Universal (CC0 1.0) Public Domain Dedication",
        "Operating System :: OS Independent",
    ],
)
