<!-- ABOUT THE PROJECT -->

## About The Project

### Key Facts

- ##### This package features two different ways of calculation emission factors for the german electricity mix.

- ##### More details on the methodology are provided in [this post](https://www.ffe.de/veroeffentlichungen/tagesaktuelle-spezifische-thg-emissionen/) (click [here for the english version](https://www.ffe.de/en/publications/tagesaktuelle-spezifische-thg-emissionen/)).

- ##### The [FfE Munich](https://www.ffe.de/) provides the results of an daily automated calculation of ex post as well as ex ante emission factors using this software package via the [FfE Open Data Platform](http://opendata.ffe.de/daily-updated-specific-greenhouse-gas-emissions-of-the-german-electricity-mix/)

#### CO2ExPost

Emission factors (ex post) are calculated by offsetting the amount of actually
produced electricity by energy source (provided by the ENTSO-E Transparency
Platform) against the specific emissions per energy source.

The historical calculations are [publicly available on the opendata platform](http://opendata.ffe.de/dataset/specific-greenhouse-gas-emissions-of-the-electricity-mix/) provided by the [FfE Munich](https://www.ffe.de/en/). This dataset is extended on a daily basis at 8:00 pm CET/CEST.

#### CO2ExAnte

This class is used for creating day-ahead forecasts of emission factors in the german electricity mix. The forecast of GHG emissions in the electricity mix is
performed using machine learning techniques. For this purpose a [Random Forest Regressor](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html) has been trained using historical day-ahead forecasts (provided by the [ENTSO-E Transparency Platform](https://transparency.entsoe.eu/)) of wind and photovoltaic generation and electricity prices as well as temporal features (e.g., day of the week). Applying the principal of supervised machine learning the ground truth of emission factors was assumed to be the result of the ex post methoded explained above. The timeframe for training data covered a period of around three years with hourly resolution.

Provided with up-to-date forecasts and additional features, the pre-trained
model is able to predict GHG-emissions in the german electricity mix for the
next day. The Notebook containing the code used for training the model is also
available in this repository.

The current day-ahead forecast is [publicly available on the opendata platform](http://opendata.ffe.de/dataset/forecast-of-specific-greenhouse-gas-emissions-of-the-electricity-mix/) provided by the [FfE Munich](https://www.ffe.de/en/). The dataset is updated daily at 10:00 pm CET/CEST.

##### The actual forecast consists of three values, which form a prediction interval:

`emission_factor_low`

:arrow_forward: this is the lower bound of the emission interval (i.e., the minimum value predicted by the random forest ensemble after the lowest 10 % of all predictions have been cut off)

`emission_factor_mean`

:arrow_forward: this is the actual prediction of the model (i.e., the mean of all values predicted by the random forest ensemble)

`emission_factor_high`

:arrow_forward: this is the upper bound of the emission interval (i.e., the maximum value predicted by the random forest ensemble after the highest 10 % of all predictions have been cut off)

### Built With

- [Python](https://www.python.org/)
- [Scikit-Learn](https://scikit-learn.org/stable/)

### Installation

install using pip

```
pip install "git+https://gitlab.com/ffe-munich/co2emissiontool.git@main"
```

install using pipenv

```
pipenv install "git+https://gitlab.com/ffe-munich/co2emissiontool.git@main#egg=co2emissiontool"
```

<!-- USAGE EXAMPLES -->

## Usage

#### Caveat

**Before the tool can be used you need to do the following:**

1.  Define the underlying GHG emission factors per energy carrier. You can use [this file](https://gitlab.com/ffe-munich/co2emissiontool/-/blob/main/specific_emission_factors.py) as a template which also contains further instructions. Also refer to [`example.py`](https://gitlab.com/ffe-munich/co2emissiontool/-/blob/main/example.py). An example of specific emission factors can be found [here](https://www.umweltbundesamt.de/sites/default/files/medien/1410/publikationen/2021-12-13_climate-change_71-2021_emissionsbilanz_erneuerbarer_energien_2020_bf_korr-01-2022.pdf).
2.  Provide the API token for the ENTSOE Transparency Platform in a file called `.entsoe_token` and located in the root directory of your procject. Alternatively, you can pass it directly when you call `CO2ExAnte` or `CO2ExPost`.

The `CO2ExPost` class can be used without restrictions and out-of-the-box. However, in order to be able perform day-ahead forecasts using the `CO2ExAnte` class, **you need to have the pre-trained random forest model stored on your machine!**. Due to file size restrictions the model is not included in this repository. If you intend to perform day-ahead forecasts, with the pre-trained model or if you want to train the model yourself using [this noteboook](https://gitlab.com/ffe-munich/co2emissiontool/-/blob/main/train_co2_forecast_random_forest_regressor.ipynb), please reach out to the contact below and we will send you the neccessary files.

The example code is also available in [example.py](https://gitlab.com/ffe-munich/co2emissiontool/-/blob/main/example.py).

```python
from datetime import date
from CO2EmissionTool.models import CO2ExAnte, CO2ExPost
from specific_emission_factors import SPECIFIC_EMISSION_FACTORS

# specify the date you are interested in
TARGET_DATE = date(2022, 3, 4)

# in this example the ENTSOE-E Token for their restful API is provided inside
# a file; alternatively you can just paste it like so:
# ENTSOE_TOKEN = '<your token goes here>'

with open('.entsoe_token', 'r') as f:
    ENTSOE_TOKEN = f.readline()

# if you want to calculate day-ahead forecasts you need to specify the path to
# the pre-trained random forest regressor
model_path = 'random_forest.pkl'

# create an instance of CO2ExAnte and provide the necessary parameters
co2_exante = CO2ExAnte(TARGET_DATE, ENTSOE_TOKEN, model_path)
# call the generate_result() method which returns the day-ahead forecast for
# the provided date
exante = co2_exante.generate_result()
print(exante.head())

# same principle applies for CO2ExPost (only that you don't need the model and you need to provide
# the specific emission factors)
co2_expost = CO2ExPost(TARGET_DATE, SPECIFIC_EMISSION_FACTORS, ENTSOE_TOKEN)
expost = co2_expost.generate_result()
print(expost.head())
```

<!-- LICENSE -->

## License

This project is licensed under the Apache License 2.0.

<!-- CONTACT -->

## Contact

Joachim Ferstl - [jferstl@ffe.de](mailto://jferstl@ffe.de)

<p align="right">(<a href="#top">back to top</a>)</p>
