"""
This module contains some constants / helpers for the CO2EmissionTool.
"""

from pandas import DataFrame, to_datetime


def add_datetime_from_epoch_idx(df: DataFrame) -> DataFrame:
    """
    This method takes a DataFrame with epoch as index and adds two datetime
    columns (datetime_utc and datetime_de) based on this index.
    :param df: input DataFrame with epoch values as index
    :return: modified DataFrame with two additional datetime columns
    """
    _df = df.copy()
    _df['datetime_utc'] = to_datetime(_df.index.to_series(), unit='s') \
        .dt.tz_localize('utc')
    _df['datetime_de'] = _df.datetime_utc.dt \
        .tz_convert('Europe/Berlin')
    return _df


# list of relevant ENSTO-E datasets
DATA_LIST = [
    'DayAheadGenerationForecastWindSolar', 'DayAheadPrices',
    'DayAheadTotalLoadForecast', 'AggregatedGenerationPerType'
]

# name of value column(s), target column names and area type per ENTSO-E
# dataset
ENTSOE_COLUMN_MAPPINGS = {
    DATA_LIST[0]: {
        'val_col': 'AggregatedGenerationForecast',
        'AreaType': 'CTY',
        'target_col': {
            'sol': 'solar',
            'w_on': 'windon',
            'w_off': 'windoff'
        }
    },
    DATA_LIST[1]: {
        'val_col': 'Price',
        'AreaType': 'BZN',
        'target_col': 'prices'
    },
    DATA_LIST[2]: {
        'val_col': 'TotalLoadValue',
        'AreaType': 'CTY',
        'target_col': 'load'
    },
    DATA_LIST[3]: {
        'val_col': 'ActualGenerationOutput',
        'AreaType': 'CTY',
        'target_col': 'gen_per_type'
    }
}

# this is the expected order of the input features for the forecast:
FORECAST_INPUT_FEATURE_ORDER = [
    'windon', 'windoff', 'solar', 'load', 'prices', 'day_of_week_fri',
    'day_of_week_sat', 'day_of_week_sun', 'day_of_week_thu', 'day_of_week_tue',
    'day_of_week_wed', 'hour_of_day_sin', 'hour_of_day_cos',
    'hour_of_year_sin', 'hour_of_year_cos'
]

# enumeration of weekdays
_weekdays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
WEEKDAY_ENUM = {i: v for i, v in enumerate(_weekdays)}

# column names of one-hot-encoded weekdays
WEEKDAY_COLUMNS = {i: f'day_of_week_{v}' for i, v in WEEKDAY_ENUM.items()}
