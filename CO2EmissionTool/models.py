"""
This module contains the main classes of the CO2EmissionTool package:

--- CO2ExAnte ---

This class is used for creating day-ahead forecasts of emission factors in the
german electricity mix. The forecast of GHG emissions in the electricity mix is
performed using machine learning techniques. For this purpose a Random Forest
Regressor has been trained using historical day-ahead forecasts of wind and
photovoltaic generation (provided by the ENTSO-E Transparency Platform) as well
as electricity prices as well as temporal features like the day of the week.
Provided with up-to-date forecasts and additional features, the pre-trained
model is able to predict GHG-emissions in the german electricity mix for the
next day. The Notebook containing the code used for training the model is also
availible in this repository.


--- CO2ExPost ---

Emission factors (ex post) are calculated by offsetting the amount of actually
produced electricity by energy source (provided by the ENTSO-E Transparency
Platform) against the specific emissions per energy source. The specific
emission factors already account for the pre-chain, which is why wind energy,
for example, also has a certain carbon footprint.
"""

from os.path import isfile
import numpy as np
from pandas import concat, DataFrame
from pickle import load
from datetime import date, timedelta
from scipy.stats import trimboth
from CO2EmissionTool._ENTSOE import fetch_wind_solar_forecasts, \
    fetch_price, fetch_production, fetch_consumption_forecast, set_token
from CO2EmissionTool._helpers import WEEKDAY_COLUMNS, \
    FORECAST_INPUT_FEATURE_ORDER, ENTSOE_COLUMN_MAPPINGS, add_datetime_from_epoch_idx


class CO2ExAnte:
    """
    This class may be used to calculate day-ahead forecasts of the CO2 emission
    factors in the german electricity mix using a pre-trained random forest
    regressor.
    """
    def __init__(self, target_date: date, entsoe_token: str, model_path: str):
        """
        | :param target_date: the day for which the day-ahead forecast of
         emission factors will be performed; this date must be at most one day
         in the future (past dates are allowed but limited to 2019 until today)
        | :param entsoe_token: token for the ENTSO-E restful API
        | :param model_path:
        """
        if not isinstance(target_date, date):
            raise TypeError('only datetime.date is accepted as "target_date"!')
        if target_date > date.today() + timedelta(days=1):
            raise ValueError(
                '"target_date" cannot be more than one day in the future!')

        self.target_date = target_date
        set_token(entsoe_token)

        # load the pre-trained random forest regressor
        if not isfile(model_path):
            raise ValueError(f'"{model_path}" is not a file')
        self._model_path = model_path
        self._model = load(open(self._model_path, 'rb'))

        # initialize additional attributes
        self.input = None
        self.result = None

    def generate_result(self) -> DataFrame:
        """
        Performs the day-ahead forecast of the emission factors and returns the
        result.
        :return: result as a pandas DataFrame
        """
        self._fetch_input_data()
        self._add_temporal_features()
        self._calculate_result()
        return self.result

    def _calculate_result(self):
        """
        This is an internal method and should not be used.
        """
        # apply pre-defined order of the features to ensure features are
        # in the order which the model expects
        input_X = self.input[FORECAST_INPUT_FEATURE_ORDER].values

        # predict emission factors and store the prediction interval in result
        lower, mean, upper = self._predict_interval(input_X)
        self.result = DataFrame(np.vstack([lower, mean, upper]).T,
                                index=self.input.index,
                                columns=[
                                    'emission_factor_low',
                                    'emission_factor_mean',
                                    'emission_factor_high'
                                ])
        self.result = add_datetime_from_epoch_idx(self.result)

    def _fetch_input_data(self):
        """
        This is an internal method and should not be used.
        """
        # get renewable production forecast from ENTSO-E
        df_re = DataFrame(
            fetch_wind_solar_forecasts('DE', target_datetime=self.target_date))
        if df_re.empty or df_re.isna().any().any():
            raise ValueError(
                'Wind / Solar forecast from ENTSO-E is empty or contains NaNs')
        df_re = concat([
            DataFrame({
                'datetime': row.datetime,
                'ProductionType': row.production.keys(),
                'val': row.production.values()
            }) for idx, row in df_re.iterrows()
        ]).reset_index(drop=True)

        # get prices forecast from ENTSO-E
        df_pr = DataFrame(fetch_price('DE', target_datetime=self.target_date))
        if df_pr.empty or df_pr.isna().any().any():
            raise ValueError(
                'Price forecast from ENTSO-E is empty or contains NaNs')

        # get load forecast from ENTSO-E
        df_ld = DataFrame(
            fetch_consumption_forecast('DE', target_datetime=self.target_date))
        if df_ld.empty or df_ld.isna().any().any():
            raise ValueError('Load forecast is empty or contains NaNs')

        # ENTSO-E API expresses time in UTC; queries contain a temporal buffer
        # and thus to many rows; keep only rows where the german local timestamp
        # matches the target date
        for _df in [df_re, df_pr, df_ld]:
            _df.drop(
                _df.loc[(_df.datetime.dt.tz_convert('Europe/Berlin').dt.date !=
                         self.target_date)].index,
                inplace=True)

        # calculate epoch (datetime column is in UTC)
        df_re['epoch_utc'] = df_re.datetime.astype('int64') // 1e9
        df_pr['epoch_utc'] = df_pr.datetime.astype('int64') // 1e9
        df_ld['epoch_utc'] = df_ld.datetime.astype('int64') // 1e9

        # delete datetime column
        for _df in [df_re, df_pr, df_ld]:
            _df.drop(columns=['datetime'], inplace=True)

        # round epoch_utc to full hours before grouping
        df_re['epoch_utc'] = df_re.epoch_utc.apply(lambda a: a - (a % 3600))
        df_pr['epoch_utc'] = df_pr.epoch_utc.apply(lambda a: a - (a % 3600))
        df_ld['epoch_utc'] = df_ld.epoch_utc.apply(lambda a: a - (a % 3600))

        # drop unnecessary columns
        df_pr = df_pr.drop(columns=['zoneKey', 'currency', 'source'])
        df_ld = df_ld.drop(columns=['zoneKey', 'source'])

        # group data by hour and aggregate values in higher temporal resolution
        # using their mean
        df_re = df_re.groupby(['epoch_utc', 'ProductionType']).mean()
        df_pr = df_pr.groupby(['epoch_utc']).mean()
        df_ld = df_ld.groupby(['epoch_utc']).mean()

        # rename the target columns and store them in seperate series
        re_target_column_names = ENTSOE_COLUMN_MAPPINGS[
            'DayAheadGenerationForecastWindSolar']['target_col']
        solar = df_re.xs('Solar',
                         level=1)['val'].rename(re_target_column_names['sol'])
        w_off = df_re.xs('Wind Offshore', level=1)['val'].rename(
            re_target_column_names['w_off'])
        w_on = df_re.xs('Wind Onshore',
                        level=1)['val'].rename(re_target_column_names['w_on'])
        prices = df_pr['price'].rename(
            ENTSOE_COLUMN_MAPPINGS['DayAheadPrices']['target_col'])
        load = df_ld['value'].rename(
            ENTSOE_COLUMN_MAPPINGS['DayAheadTotalLoadForecast']['target_col'])

        self.input = concat([w_on, w_off, solar, load, prices], axis=1)

        # add timestamps in utc / german time
        self.input = add_datetime_from_epoch_idx(self.input)

    def _add_temporal_features(self):
        """
        This is an internal method and should not be used.
        """
        # add one-hot-encoded day of week
        # append empty columns for weekdays
        self.input = self.input.reindex(columns=list(self.input.columns) +
                                        list(WEEKDAY_COLUMNS.values()),
                                        fill_value=0)
        # fill corresponding columns with 1 according to the weekday of
        # the target date
        self.input[WEEKDAY_COLUMNS[self.target_date.weekday()]] = 1
        # drop one of the weekdays in order to avoid multicolinearity
        self.input.drop(columns=['day_of_week_mon'], inplace=True)

        self.input['hour_of_day'] = self.input.datetime_de.dt.hour

        # timezone change not considered, but ignored since not too relevant
        self.input['hour_of_year'] = (
            (self.input.datetime_de.dt.dayofyear - 1) *
            24) + self.input.datetime_de.dt.hour

        # convert cyclic temporal features
        c_features = ['hour_of_day', 'hour_of_year']
        c_numbers = [24, 8760]
        for f, n in zip(c_features, c_numbers):
            self.input[f + '_sin'] = np.sin(self.input[f] * (2. * np.pi / n))
            self.input[f + '_cos'] = np.cos(self.input[f] * (2. * np.pi / n))

    def _predict_interval(self, X, trim=.1):
        """
        This is an internal method and should not be used.
        """
        """
        This method predicts the target individually for each estimator of the 
        random forest regressor. The results are trimmed to both sides by the 
        given percentage. The method returns a lower bound, upper bound as well 
        as the average prediction of the ensemble model.
        https://blog.datadive.net/prediction-intervals-for-random-forests/
        """
        err_down = []
        err_up = []
        avg_pred = []
        for x in range(len(X)):
            preds = []
            for pred in self._model.estimators_:
                preds.append(pred.predict(X[x].reshape(1, -1))[0])
            # trim the predictions array
            preds = trimboth(preds, trim)

            # append prediction interval to result
            err_down.append(np.min(preds))
            avg_pred.append(np.mean(preds))
            err_up.append(np.max(preds))

        return np.array(err_down), np.array(avg_pred), np.array(err_up)


class CO2ExPost:
    """
    This class may be used to calculate the CO2 emission factors in the german
    electricity mix for a given day based on actual produced energy by energy
    source (provided by the ENTSO-E Transparency Platform) in combination with
    specific emission factors per energy source.
    """
    def __init__(self, target_date: date, spec_emission_facotrs: DataFrame, entsoe_token: str):
        """
        | :param target_date: the day for which the emissions will be
          calculated; this date must be in the past
        | :param entsoe_token: token for the ENTSO-E restful API
        """
        if not isinstance(target_date, date):
            raise TypeError('only datetime.date is accepted as "target_date"!')
        if target_date >= date.today():
            raise ValueError(
                '"target_date" must be at least one day in the past!')
        self.target_date = target_date
        set_token(entsoe_token)

        # initialize additional attributes
        self.input = None
        self.result = None

        # initialize the specific emission factors
        self.spec_emission_factors = spec_emission_facotrs

    def generate_result(self) -> DataFrame:
        """
        Performs the calculation of the emission factors and returns them.
        :return: result as a pandas DataFrame
        """
        if not self.input:
            self._fetch_input_data()
        self._calculate_result()
        return self.result

    def _calculate_result(self):
        """
        This is an internal method and should not be used.
        """

        df = self.input.copy()

        # convert MWh to kWh
        df['gen_per_type_mw'] = df['gen_per_type_mw'] * 1000

        # calculate fraction per hour for each energy carrier
        # kWh
        df['total_energy_per_hour'] = df.groupby('epoch_utc')['gen_per_type_mw'] \
            .transform('sum')

        # make all lower case and replace spaces with '-'
        df['productiontype'] = df['productiontype'] \
            .apply(lambda r: r.lower().replace(' ', '-'))

        # join sprecific emission factors
        df = df.reset_index() \
            .merge(right=self.spec_emission_factors,
                   how='left',
                   left_on='productiontype',
                   right_on='et_entsoe') \
            .filter(items=[
            'epoch_utc', 'productiontype', 'gen_per_type_mw',
            'total_energy_per_hour', 'frac', 'spec_emission'
        ])

        # calculate absolute emissions per energy carrier and hour
        # kWh * kg_CO2_eq / kWh --> kg_CO2_eq
        df['abs_emission_per_type'] = df['gen_per_type_mw'] * df[
            'spec_emission']

        # calculate sum of absolute emissions per hour
        # kg_CO2_eq
        df['sum_abs_emission_per_hour'] = df.groupby(
            'epoch_utc')['abs_emission_per_type'].transform('sum')

        # divide sum of absolute emission per hour by the sum of absolute
        # energy per hour to get specific emissions per hour; multiply by
        # 1000 to get units of "g_CO2_eq / kWh"
        df['emission_factor'] = (df['sum_abs_emission_per_hour'] /
                                 df['total_energy_per_hour']) * 1000

        # store results for current date in results list
        self.result = df.groupby(['epoch_utc', 'emission_factor']) \
            .sum() \
            .reset_index() \
            .filter(items=['epoch_utc', 'emission_factor']) \
            .set_index('epoch_utc') \
            .sort_index()

        # add timestamps in german time; epoch is in utc
        self.result = add_datetime_from_epoch_idx(self.result)

    def _fetch_input_data(self):
        """
        This is an internal method and should not be used.
        """

        p = ENTSOE_COLUMN_MAPPINGS['AggregatedGenerationPerType']

        # fetch data from ENSTO Transparency Platform using the ENTSO-E
        # parser from https://github.com/tmrowco/electricitymap-contrib
        df = DataFrame(fetch_production('DE',
                                        target_datetime=self.target_date))
        if df.empty or df.isna().any().any():
            raise ValueError(
                'Production data from ENTSO-E is empty or contains NaNs')
        df = concat([
            DataFrame({
                'datetime': row.datetime,
                'ProductionType': row.production.keys(),
                'val': row.production.values()
            }) for idx, row in df.iterrows()
        ]).reset_index(drop=True)

        # ENTSO-E API expresses time in UTC; queries contain a temporal buffer
        # and thus to many rows; keep only rows where the german local timestamp
        # matches the target date
        df.drop(df.loc[(df.datetime.dt.tz_convert('Europe/Berlin').dt.date !=
                        self.target_date)].index,
                inplace=True)

        # calculate epoch (datetime column is in UTC)
        df['epoch_utc'] = df.datetime.astype('int64') // 1e9

        # delete datetime column
        df.drop(columns=['datetime'], inplace=True)

        # round epoch_utc to full hours before grouping
        df['epoch_utc'] = df.epoch_utc.apply(lambda a: a - (a % 3600))

        # fill na
        df['val'] = df['val'].fillna(0)

        # values are MWh after this operation
        df = df.dropna() \
            .groupby(['epoch_utc', 'ProductionType']) \
            .mean()['val'] \
            .rename(p['target_col']) \
            .reset_index()

        self.input = df.sort_values(by=['epoch_utc', 'ProductionType']) \
            .set_index('epoch_utc')

        # some renaming
        self.input = self.input.rename(columns={
            'ProductionType': 'productiontype',
            'gen_per_type': 'gen_per_type_mw'
        })

        # add timestamps in utc / german time
        self.input = add_datetime_from_epoch_idx(self.input)
