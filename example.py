from datetime import date
from CO2EmissionTool.models import CO2ExAnte, CO2ExPost
from specific_emission_factors import SPECIFIC_EMISSION_FACTORS

# specify the date you are interested in
TARGET_DATE = date(2022, 3, 4)

# in this example the ENTSOE-E Token for their restful API is provided inside
# a file; alternatively you can just paste it like so:
# ENTSOE_TOKEN = '<your token goes here>'

with open('.entsoe_token', 'r') as f:
    ENTSOE_TOKEN = f.readline()

# if you want to calculate day-ahead forecasts you need to specify the path to
# the pre-trained random forest regressor
model_path = 'random_forest.pkl'

# create an instance of CO2ExAnte and provide the necessary parameters
co2_exante = CO2ExAnte(TARGET_DATE, ENTSOE_TOKEN, model_path)
# call the generate_result() method which returns the day-ahead forecast for
# the provided date
exante = co2_exante.generate_result()
print(exante.head())

# same principle applies for CO2ExPost (only that you don't need the model and you need to provide
# the specific emission factors)
co2_expost = CO2ExPost(TARGET_DATE, SPECIFIC_EMISSION_FACTORS, ENTSOE_TOKEN)
expost = co2_expost.generate_result()
print(expost.head())
