"""
This module defines the specific emission factors per energy carrier (in kg_CO2_eq / kWh_el).
The names of the energy carriers are derived from ENTSO-E naming.

Since the emission factors used for training the model are not publicly available, these need to be
specified by the user.

Everytime you create an instance of CO2ExPost, besides the target data and the API token for ENTSOE
you need to provide the DataFrame produced in this module as "spec_emission_factors" like this:

co2_expost = CO2ExPost(target_date=TARGET_DATE,
                       spec_emission_factors=SPECIFIC_EMISSION_FACTORS,
                       entsoe_token=ENTSOE_TOKEN)
"""

from pandas import DataFrame


spec_emission_factors = [{
    "et_entsoe": "Wind Onshore",
    "spec_emission": 0
}, {
    "et_entsoe": "Solar",
    "spec_emission": 0
}, {
    "et_entsoe": "Wind Offshore",
    "spec_emission": 0
}, {
    "et_entsoe": "Geothermal",
    "spec_emission": 0
}, {
    "et_entsoe": "Biomass",
    "spec_emission": 0
}, {
    "et_entsoe": "Nuclear",
    "spec_emission": 0
}, {
    "et_entsoe": "Fossil Hard coal",
    "spec_emission": 0
}, {
    "et_entsoe": "Fossil Brown coal/Lignite",
    "spec_emission": 0
}, {
    "et_entsoe": "Waste",
    "spec_emission": 0
}, {
    "et_entsoe": "Fossil Oil",
    "spec_emission": 0
}, {
    "et_entsoe": "Fossil Gas",
    "spec_emission": 0
}, {
    "et_entsoe": "Fossil Peat",
    "spec_emission": 0
}, {
    "et_entsoe": "Other renewable",
    "spec_emission": 0
}, {
    "et_entsoe": "Hydro Run-of-river and poundage",
    "spec_emission": 0
}, {
    "et_entsoe": "Hydro Water Reservoir",
    "spec_emission": 0
}, {
    "et_entsoe": "Hydro Pumped Storage",
    "spec_emission": 0
}, {
    "et_entsoe": "Other",
    "spec_emission": 0
}, {
    "et_entsoe": "Marine",
    "spec_emission": 0
}, {
    "et_entsoe": "Fossil Coal-derived gas",
    "spec_emission": 0
}, {
    "et_entsoe": "Fossil Oil shale",
    "spec_emission": 0
}, {
    "et_entsoe": "Gas chp public",
    "spec_emission": 0
}, {
    "et_entsoe": "Gas chp industry",
    "spec_emission": 0
}, {
    "et_entsoe": "Hard coal chp public",
    "spec_emission": 0
}]

SPECIFIC_EMISSION_FACTORS = DataFrame(spec_emission_factors)
# make all lower case and replace spaces with '-'
SPECIFIC_EMISSION_FACTORS['et_entsoe'] = SPECIFIC_EMISSION_FACTORS[
    'et_entsoe'].apply(lambda r: r.lower().replace(' ', '-'))
